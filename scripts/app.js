$(function () {
    window.app = new function () {
        this.body = $('body');
        $('#lightSwitch').on('click', $.proxy(function () {
            this.body.toggleClass('dark');

            $('.ticket').each(function (index) {
                $(this).find('.ticket__title').toggleClass('ticket__title--dark');
                $(this).find('.ticket__message').toggleClass('ticket__message--dark');
                $(this).find('.ticket__round-item').toggleClass('ticket__round-item--dark')
            });
        }, this));

        $('button').on('click', function () {
            var text = $('#MessageInput').val().trim();

            if (text !== '') {
                messageControls.showMessage(text);
            } else {
                alert('You should input text!');
            }
        });
    }();

    // Module for message controls
    var messageControls = function () {
        var cloneLight = $('.ticket__clone'),
            cloneDark = $('.ticket-dark__clone');

        var cloneTicketLight = cloneLight
            .clone()
            .removeClass('ticket__clone');

        var cloneTicketDark = cloneDark
            .clone()
            .removeClass('.ticket-dark__clone');

        cloneLight.remove();
        cloneDark.remove();

        var ticketsLine = $('#tickets-line');

        var qMsg = [],
            block = false;

        function appendTicket(element) {
            var countTickets = ticketsLine.find('.ticket').length + 1;

            var sumHeight = 0;

            ticketsLine.append(
                element
                    .clone()
                    .css({
                        "position": "absolute",
                        "bottom": 0
                    })
                    .show(function () {
                            for (var i = 0; i < $('.ticket').length; i++) {
                                sumHeight += $('.ticket').eq(i).outerHeight();
                            }

                            $(this).animate({
                                "bottom": $('body').height() - sumHeight
                            }, 300, function () {
                                clearTickets();
                            });
                        }
                    )
            );
        }

        function clearTickets() {
            var tickets = ticketsLine.find('.ticket'),
                coords = [];

            for (var i = 0; i < tickets.length; i++) {
                coords.push((tickets.eq(i).position().top + tickets.eq(i).outerHeight()));
            }

            if (tickets.last().position().top >= $('body').height() / 2 && tickets.length !== 1) {
                tickets.eq(0).remove();

                for (var j = 1; j < coords.length; j++) {
                    var sumHeight = 0;
                    for (var i = 0; i < j; i++) {
                        sumHeight += $('.ticket').eq(i).outerHeight();
                    }

                    tickets
                        .eq(j)
                        .animate({
                            "bottom": $('body').height() - sumHeight
                        }, 300);
                }
            }

            block = false;
        }

        return {
            showMessage: function (text) {
                qMsg.push(text);

                while (!block && qMsg.length !== 0) {
                    block = true;

                    if ($('body').hasClass('dark')) {
                        cloneTicketDark
                            .find('.ticket__text')
                            .text(qMsg.pop());

                        appendTicket(cloneTicketDark);
                    } else {
                        cloneTicketLight
                            .find('.ticket__text')
                            .text(qMsg.pop());

                        appendTicket(cloneTicketLight);
                    }
                }
            }
        }
    }();
})
;